Work with classes and objects in Python
=============================

This is a walkthrough on how to work with classes and objects in Python using existing code examples.

It is assumed that you are familiar with the basic concepts of programming in an object oriented programming style. 

[[_TOC_]]



References
-----------------------------

* [Python classes](https://docs.python.org/3/tutorial/classes.html#classes)
